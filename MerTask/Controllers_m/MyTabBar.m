//
//  UITabBarController+myTabBar.m
//  MerTask
//
//  Created by Kriss Violense on 26.02.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import "MyTabBar.h"
#import "WebViewController.h"
#import "MyTableViewController.h"



@interface MyTabBar ()

@end


@implementation MyTabBar

-(id) init {
    self = [super init];
    
    _vc1 = [[UINavigationController alloc] init];
    _vc2 = [[UINavigationController alloc] init];
    
    WebViewController *webView = [[WebViewController alloc] init];
    [_vc2 addChildViewController:webView];
    
    
    MyTableViewController *table = [[MyTableViewController alloc] init];
    [_vc1 addChildViewController:table];
    [table release];
    
    NSMutableArray *tabVC = [[[NSMutableArray alloc] init] autorelease];
    [tabVC addObject:_vc1];
    [tabVC addObject:_vc2];
    
    [self setViewControllers:tabVC];
    
    _vc1.tabBarItem = [[[UITabBarItem alloc] initWithTitle:@"TableView" image:[UIImage imageNamed:@"rest.png"] selectedImage:[UIImage imageNamed:@"rest.png"]] autorelease];
    
    _vc2.tabBarItem = [[[UITabBarItem alloc] initWithTitle:@"WebView" image:[UIImage imageNamed:@"email.png"] selectedImage:[UIImage imageNamed:@"email.png"]] autorelease];
    
    [_vc1 release];
    [_vc2 release];
    return self;
}

@end

