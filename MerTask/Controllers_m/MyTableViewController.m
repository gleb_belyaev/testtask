//
//  MyTableViewController.m
//  MerTask
//
//  Created by Kriss Violense on 02.03.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import "MyTableViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "MoreInfo.h"

@interface UITableViewController ()


@end


@implementation MyTableViewController : UITableViewController

-(id) init {
    self = [super init];
    responseJSON = [[NSMutableArray alloc] init];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self getRequestRestaurant];
    });
    
    
    return self;
}

-(void) getRequestRestaurant
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
    [manager GET:@"http://sandbox.bottlerocketapps.com/BR_iOS_CodingExam_2015_Server/restaurants.json" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        responseDict = responseObject;
        [self.tableView reloadData];
        [self mappingJSON : responseDict];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}


- (void)mappingJSON:(NSDictionary *)dictionary
{
    NSArray * myAllKeys =  [dictionary objectForKey:@"restaurants"];
    for (int i = 0; i < [myAllKeys count]; ++i)
    {
        Restaurant *restauran = [[Restaurant alloc] init];
        [Restaurant parseJSON:restauran : myAllKeys[i]];
        [responseJSON addObject:restauran];
        [restauran release];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 15;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}


- (void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSUInteger const kaddressLabelTag = 1;
    static NSUInteger const kLogoLabelTag = 2;
    static NSUInteger const kphoneLabelTag = 3;
    static NSUInteger const kBlureLabelTag = 2;
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UILabel *logoLabel;
    UILabel *blureLabel;
    UILabel *phoneLabel;
    UILabel *addressLabel;
    
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGRect blureFrame = CGRectMake(0, 0, tableView.contentSize.width, 60);
        CGRect logoFrame = CGRectMake(0, 0, tableView.contentSize.width, 58);
        CGRect phoneFrame = CGRectMake(20, 10, 300, 16);
        CGRect addressFrame = CGRectMake(20, 40, 300, 16);
        
        logoLabel = [[[UILabel alloc] initWithFrame:logoFrame] autorelease];
        logoLabel.tag = kLogoLabelTag;
        
        blureLabel = [[[UILabel alloc] initWithFrame:blureFrame] autorelease];
        blureLabel.tag = kBlureLabelTag;
        
        phoneLabel = [[[UILabel alloc] initWithFrame:phoneFrame] autorelease];
        phoneLabel.tag = kphoneLabelTag;
        phoneLabel.backgroundColor = [UIColor clearColor];
        phoneLabel.textColor = [UIColor whiteColor];
        phoneLabel.textAlignment = NSTextAlignmentLeft;
        
        addressLabel = [[[UILabel alloc] initWithFrame:addressFrame] autorelease];
        addressLabel.tag = kaddressLabelTag;
        addressLabel.backgroundColor = [UIColor clearColor];
        addressLabel.textColor = [UIColor whiteColor];
        addressLabel.textAlignment = NSTextAlignmentLeft;
        
        [cell.contentView addSubview:logoLabel];
        [cell.contentView addSubview:blureLabel];
        [cell.contentView addSubview:phoneLabel];
        [cell.contentView addSubview:addressLabel];
        
    }
    else
    {
        logoLabel = (UILabel *) [cell.contentView viewWithTag:kLogoLabelTag];
        blureLabel = (UILabel *) [cell.contentView viewWithTag:kLogoLabelTag];
        phoneLabel = (UILabel *) [cell.contentView viewWithTag:kphoneLabelTag];
        addressLabel = (UILabel *) [cell.contentView viewWithTag:kaddressLabelTag];
    }
    
    
    logoLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"placeholder.png"]];
    blureLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6f];
    
    if (responseJSON.count)
    {
        
        Restaurant *tmp = responseJSON[indexPath.row];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:tmp.backgroundImageURL]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                                 
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    logoLabel.backgroundColor = [UIColor colorWithPatternImage:image];
                                }
                            }];
        
        
        if ([tmp.contact.formattedPhone  isEqual: @""])
            phoneLabel.text = @"unknown phone number";
        else
            phoneLabel.text = tmp.contact.formattedPhone;
        addressLabel.text = tmp.location.address;
        
        
        //        NSLog(@" Name : %@  - %@ | %@", tmp.name, tmp.contact.formattedPhone, tmp.location.address);
    }
    return cell;
}

- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
    if(responseJSON.count)
    {
        [self createMoreInfo:responseJSON[indexPath.row]];
    }
    NSLog(@"%ld", (long)indexPath.row);
}


- (void)createMoreInfo: (Restaurant *)restoranData  {
    MoreInfo *infoVC = [[[MoreInfo alloc] init] autorelease];
    [infoVC setAllInfo: restoranData];
    [self.navigationController pushViewController:infoVC animated:NO];
}

@end
