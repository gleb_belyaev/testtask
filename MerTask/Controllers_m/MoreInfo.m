//
//  MoreInfo.m
//  MerTask
//
//  Created by Kriss Violense on 09.03.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import "MoreInfo.h"

@implementation MoreInfo

- (void) setAllInfo:(Restaurant *)rest {
    self.title = rest.name;
    
    int x = 70;
    int step = 30;
    
    UILabel *blureLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, self.view.bounds.size.width, 160)];
    blureLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6f];
    
    
    UILabel *logoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, self.view.bounds.size.width, 160)];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:rest.backgroundImageURL]
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                logoLabel.backgroundColor = [UIColor colorWithPatternImage:image];
                            }
                        }];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, x, self.view.bounds.size.width, 20)];
    nameLabel.text = [NSString stringWithFormat:@"%@", @"name : "];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.textAlignment = NSTextAlignmentLeft;
    
    UILabel *name_lbl = [[UILabel alloc] initWithFrame:CGRectMake(100, x, self.view.bounds.size.width - 110, 20)];
    name_lbl.text = [NSString stringWithFormat:@"%@", rest.name];
    name_lbl.backgroundColor = [UIColor clearColor];
    name_lbl.textColor = [UIColor whiteColor];
    name_lbl.textAlignment = NSTextAlignmentLeft;
    x += step;
    
    UILabel *categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, x, self.view.bounds.size.width, 20)];
    categoryLabel.text = [NSString stringWithFormat:@"%@", @"category : "];
    categoryLabel.backgroundColor = [UIColor clearColor];
    categoryLabel.textColor = [UIColor whiteColor];
    categoryLabel.textAlignment = NSTextAlignmentLeft;
    
    UILabel *category_lbl = [[UILabel alloc] initWithFrame:CGRectMake(100, x, self.view.bounds.size.width - 110, 20)];
    category_lbl.text = [NSString stringWithFormat:@"%@", rest.category];
    category_lbl.backgroundColor = [UIColor clearColor];
    category_lbl.textColor = [UIColor whiteColor];
    category_lbl.textAlignment = NSTextAlignmentLeft;
    x += step;
    
    UILabel *contactLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, x, self.view.bounds.size.width - 10, 20)];
    contactLabel.text = [NSString stringWithFormat:@"%@", @"contact : "];
    contactLabel.backgroundColor = [UIColor clearColor];
    contactLabel.textColor = [UIColor whiteColor];
    contactLabel.textAlignment = NSTextAlignmentLeft;
    
    [self.view addSubview:logoLabel];
    [self.view addSubview:blureLabel];
    [self.view addSubview:nameLabel];
    [self.view addSubview:name_lbl];
    [self.view addSubview:categoryLabel];
    [self.view addSubview:category_lbl];
    [self.view addSubview:contactLabel];
    
    if (![rest.contact.formattedPhone  isEqual: @""])
    {
        UILabel *phone_lbl = [[UILabel alloc] initWithFrame:CGRectMake(130, x, self.view.bounds.size.width - 140, 20)];
        phone_lbl.text = [NSString stringWithFormat:@"%@", rest.contact.formattedPhone];
        phone_lbl.backgroundColor = [UIColor clearColor];
        phone_lbl.textColor = [UIColor whiteColor];
        phone_lbl.textAlignment = NSTextAlignmentLeft;
        
        UIImageView *phone_icon = [[UIImageView alloc] initWithFrame:CGRectMake(100, x, 20, 20)];
        phone_icon.image = [UIImage imageNamed:@"phone.png"];
        [self.view addSubview:phone_icon];
        
        [self.view addSubview:phone_lbl];
        [phone_icon release];
        [phone_lbl release];
        x += step;
    }
    
    if (![rest.contact.twitter  isEqual: @""])
    {
        UILabel *twitter_lbl = [[UILabel alloc] initWithFrame:CGRectMake(130, x, self.view.bounds.size.width - 140, 20)];
        twitter_lbl.text = [NSString stringWithFormat:@"%@", rest.contact.twitter];
        twitter_lbl.backgroundColor = [UIColor clearColor];
        twitter_lbl.textColor = [UIColor whiteColor];
        twitter_lbl.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:twitter_lbl];
        
        UIImageView *twitter_icon = [[UIImageView alloc] initWithFrame:CGRectMake(100, x, 20, 20)];
        twitter_icon.image = [UIImage imageNamed:@"twitter.png"];
        
        [self.view addSubview:twitter_icon];
        
        
        [twitter_lbl release];
        [twitter_icon release];
        x += step;
        
    }
    
    if (![rest.contact.facebook  isEqual: @""])
    {
        UILabel *facebook_lbl = [[UILabel alloc] initWithFrame:CGRectMake(130, x, self.view.bounds.size.width - 140, 20)];
        facebook_lbl.text = [NSString stringWithFormat:@"%@", rest.contact.facebookName];
        facebook_lbl.backgroundColor = [UIColor clearColor];
        facebook_lbl.textColor = [UIColor whiteColor];
        facebook_lbl.textAlignment = NSTextAlignmentLeft;
        
        UIImageView *facebook_icon = [[UIImageView alloc] initWithFrame:CGRectMake(100, x, 20, 20)];
        facebook_icon.image = [UIImage imageNamed:@"facebook.png"];
        
        [self.view addSubview:facebook_icon];
        [self.view addSubview:facebook_lbl];
        [facebook_icon release];
        [facebook_lbl release];
        x += step;
    }
    
    GMSCameraPosition *camera_ = [GMSCameraPosition cameraWithLatitude:[rest.location.lat floatValue]
                                                            longitude:[rest.location.lat floatValue]
                                                                 zoom:6];
    
    GMSMapView *mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, x, self.view.bounds.size.width, self.view.bounds.size.height - 160) camera:camera_];
    mapView_.myLocationEnabled = YES;
    [self.view addSubview:mapView_];
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[[GMSMarker alloc] init] autorelease];
    marker.position = CLLocationCoordinate2DMake([rest.location.lat floatValue], [rest.location.lat floatValue]);
    marker.title = rest.name;
    marker.snippet = rest.location.formattedAddress[1];
    marker.map = mapView_;
    
    [logoLabel release];
    [blureLabel release];
    [nameLabel release];
    [name_lbl release];
    [categoryLabel release];
    [category_lbl release];
    [contactLabel release];
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
}

- (void) viewDidLoad{
    [super viewDidLoad];
    
    
}

@end
