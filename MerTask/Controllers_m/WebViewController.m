//
//  UIViewController+WebViewController.m
//  MerTask
//
//  Created by Kriss Violense on 26.02.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController () <UIWebViewDelegate>

- (void)loadRequestFromAddressField:(id)addressField;
- (void)loadRequestFromString:(NSString*)urlString;

- (void) updateButtons;

@end


static const CGFloat kMargin = 10.0f;
static const CGFloat kSpacer = 10.0f;
static const CGFloat kAddressHeight = 24.0f;

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect forWebView = self.view.bounds;
    self.webView = [[UIWebView alloc] initWithFrame: forWebView];
    
    _toolbar = [[[UIToolbar alloc] init] autorelease];
    _toolbar.frame = CGRectMake(0, self.view.frame.size.height - 90 , self.view.frame.size.width, 44);
    NSMutableArray *items = [[NSMutableArray alloc] init];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self.webView action:nil];
    self.back = [[UIBarButtonItem alloc] initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self.webView action:@selector(goBack)];
    self.refresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self.webView action:@selector(reload)];
    self.forward = [[UIBarButtonItem alloc] initWithTitle:@">" style:UIBarButtonItemStylePlain target:self.webView action:@selector(goForward)];
    
    [items addObject:self.back];
    [items addObject:spacer];
    [items addObject:self.refresh];
    [items addObject:spacer];
    [items addObject:self.forward];
    
    [_toolbar setItems:items animated:NO];
    [self.view addSubview:_toolbar];
    [spacer release];
    [items release];
    
    
    UINavigationBar *navBar = self.navigationController.navigationBar;
    CGRect addressFrame = CGRectMake(kMargin, kSpacer,
                                     navBar.bounds.size.width - 2*kMargin, kAddressHeight);
    
    UITextField *address = [[UITextField alloc] initWithFrame:addressFrame];
    address.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    address.borderStyle = UITextBorderStyleRoundedRect;
    address.font = [UIFont systemFontOfSize:17];
    [address addTarget:self
                action:@selector(loadRequestFromAddressField:)
      forControlEvents:UIControlEventEditingDidEndOnExit];
    [navBar addSubview:address];
    self.addressField = address;
    
    self.webView.delegate = self;
    [self loadRequestFromString:@"http://www.apple.com/"];
    
    [self updateButtons];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)updateButtons {
    self.forward.enabled = self.webView.canGoForward;
    self.back.enabled = self.webView.canGoBack;
    self.stop.enabled = self.webView.loading;
}


- (void)loadRequestFromAddressField:(id)tmpAddressField
{
    NSString *urlString = [tmpAddressField text];
    NSString* result;
    // проверка на http
    if (![urlString containsString:@"http://"]) {
        result = [@"http://" stringByAppendingString:urlString];
    }
    else {
        result = urlString;
    }
    [self.view addSubview:self.webView ];
    [self loadRequestFromString:result];
}

- (void)loadRequestFromString:(NSString*)urlString
{
    _addressField.text = urlString;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.view addSubview:self.webView ];
    [self.webView loadRequest:urlRequest];
    [self.view addSubview:_toolbar];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self updateButtons];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    _addressField.text = webView.request.URL.absoluteString;
    [self updateButtons];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self updateButtons];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}


@end

