//
//  Restaurant.h
//  MerTask
//
//  Created by Kriss Violense on 02.03.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestaurantCell: NSObject

@property (nonatomic, copy)     NSString *phone;
@property (nonatomic, copy)     NSString *backgroundImageURL;
@property (nonatomic, copy)     NSString *address;
    
@end


@interface Contact : NSObject

@property (nonatomic, copy)     NSString *phone;
@property (nonatomic, copy)     NSString *formattedPhone;
@property (nonatomic, copy)     NSString *twitter;
@property (nonatomic, copy)     NSString *facebook;
@property (nonatomic, copy)     NSString *facebookUsername;
@property (nonatomic, copy)     NSString *facebookName;

+ (void) parseJSON: (Contact *) contact : (NSDictionary *) dictionary;


@end


@interface Location : NSObject
@property (nonatomic, copy)   NSString *crossStreet;
@property (nonatomic, copy)   NSString *lat;
@property (nonatomic, copy)   NSString *lng;
@property (nonatomic, copy)   NSString *postalCode;
@property (nonatomic, copy)   NSString *cc;
@property (nonatomic, copy)   NSString *city;
@property (nonatomic, copy)   NSString *state;
@property (nonatomic, copy)   NSString *country;
@property (nonatomic, copy)   NSArray *formattedAddress;
@property (nonatomic, copy)   NSString *address;

+ (void) parseJSON: (Location *) location :(NSDictionary *) dictionary;

@end


@interface Restaurant: NSObject

+ (void) parseJSON: (Restaurant *) rest :(NSDictionary *) dictionary;

@property (nonatomic, copy)   NSString *name;
@property (nonatomic, copy)   NSString *backgroundImageURL;
@property (nonatomic, copy)   NSString *category;
@property (nonatomic, strong)   Contact *contact;
@property (nonatomic, strong)   Location *location;

@end

