//
//  UIViewController+WebViewController.h
//  MerTask
//
//  Created by Kriss Violense on 26.02.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong)     UIWebView *webView;
@property (nonatomic, strong)     UIBarButtonItem *back;
@property (nonatomic, strong)     UIBarButtonItem *stop;
@property (nonatomic, strong)     UIBarButtonItem *refresh;
@property (nonatomic, strong)     UIBarButtonItem *forward;
@property (nonatomic, strong)     UILabel *pageTitle;
@property (nonatomic, strong)     UITextField *addressField;
@property (nonatomic, strong)     UIToolbar *toolbar;

@end
