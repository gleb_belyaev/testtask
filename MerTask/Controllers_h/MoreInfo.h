//
//  MoreInfo.h
//  MerTask
//
//  Created by Kriss Violense on 09.03.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <GoogleMaps/GoogleMaps.h>


@interface MoreInfo : UIViewController

- (void) setAllInfo : (Restaurant *)dicrtions;

@end
