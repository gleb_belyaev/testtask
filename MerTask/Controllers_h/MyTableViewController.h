//
//  MyTableViewController.h
//  MerTask
//
//  Created by Kriss Violense on 02.03.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"


@interface MyTableViewController : UITableViewController
{
    NSDictionary *responseDict;
    NSMutableArray * responseJSON;
}

- (void) getRequestRestaurant;
- (void) mappingJSON: (NSDictionary *) dictionar;
- (void) createMoreInfo: (Restaurant *)restoranData;

@property (strong, nonatomic) UIWindow *window;

@end
