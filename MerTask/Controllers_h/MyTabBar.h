//
//  UITabBarController+myTabBar.h
//  MerTask
//
//  Created by Kriss Violense on 26.02.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTabBar : UITabBarController

@property (strong, nonatomic) UINavigationController *vc1;
@property (strong, nonatomic) UINavigationController *vc2;
@property (strong, nonatomic) UIWindow *window;

@end
