//
//  Restaurant.m
//  MerTask
//
//  Created by Kriss Violense on 02.03.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import "Restaurant.h"

@interface NSObject ()

@end


@implementation Restaurant : NSObject

+ (void) parseJSON: (Restaurant *) rest : (NSDictionary *) dictionary
{
    rest.name = [dictionary objectForKey:@"name"];
    rest.backgroundImageURL = [dictionary objectForKey:@"backgroundImageURL"];
    rest.category = [dictionary objectForKey:@"category"];
    
    Contact *contact = [[[Contact alloc] init] autorelease];
    [Contact parseJSON:contact : dictionary];
    rest.contact = contact;
    
    Location *location = [[[Location alloc]init] autorelease];
    [Location parseJSON:location : dictionary];
    rest.location = location;
    
}

@end

@implementation Contact : NSObject

+ (void) parseJSON: (Contact *) contact : (NSDictionary *) dictionary
{
    if ([dictionary objectForKey:@"contact"] != [NSNull null])
    {
        NSDictionary * contactDictionary = [dictionary objectForKey:@"contact"];
        if ([contactDictionary objectForKey:@"phone"])
            contact.phone = [contactDictionary objectForKey:@"phone"];
        else
            contact.phone = @"";
        
        if ([contactDictionary objectForKey:@"formattedPhone"])
            contact.formattedPhone = [contactDictionary objectForKey:@"formattedPhone"];
        else
            contact.formattedPhone = @"";
        
        if ([contactDictionary objectForKey:@"twitter"])
            contact.twitter = [contactDictionary objectForKey:@"twitter"];
        else
            contact.twitter = @"";
        
        if ([contactDictionary objectForKey:@"facebook"])
            contact.facebook = [contactDictionary objectForKey:@"facebook"];
        else
            contact.facebook = @"";
        
        if ([contactDictionary objectForKey:@"facebookUsername"])
            contact.facebookUsername = [contactDictionary objectForKey:@"facebookUsername"];
        else
            contact.facebookUsername = @"";
        
        if ([contactDictionary objectForKey:@"facebookName"])
            contact.facebookName = [contactDictionary objectForKey:@"facebookName"];
        else
            contact.facebookName = @"";
        
    }
    else
    {
        contact.phone = @"";
        contact.formattedPhone = @"";
        contact.twitter = @"";
        contact.facebook = @"";
        contact.facebookUsername = @"";
        contact.facebookName = @"";
    }
    
    
}

@end

@implementation Location : NSObject

+ (void) parseJSON: (Location *) location :(NSDictionary *) dictionary;
{
    if ([dictionary objectForKey:@"location"] != [NSNull null])
    {
        NSDictionary * locationDictionary = [dictionary objectForKey:@"location"];
        if ([locationDictionary objectForKey:@"address"])
            location.address = [locationDictionary objectForKey:@"address"];
        else
            location.address = @"";
        
        if ([locationDictionary objectForKey:@"crossStreet"])
            location.crossStreet = [locationDictionary objectForKey:@"crossStreet"];
        else
            location.crossStreet = @"";
        
        if ([locationDictionary objectForKey:@"lat"])
            location.lat = [locationDictionary objectForKey:@"lat"];
        else
            location.lat = @"";
        
        if ([locationDictionary objectForKey:@"lng"])
            location.lng = [locationDictionary objectForKey:@"lng"];
        else
            location.lng = @"";
        
        if ([locationDictionary objectForKey:@"postalCode"])
            location.postalCode = [locationDictionary objectForKey:@"postalCode"];
        else
            location.postalCode = @"";
        
        if ([locationDictionary objectForKey:@"cc"])
            location.cc = [locationDictionary objectForKey:@"cc"];
        else
            location.cc = @"";
        
        if ([locationDictionary objectForKey:@"city"])
            location.city = [locationDictionary objectForKey:@"city"];
        else
            location.city = @"";
        
        if ([locationDictionary objectForKey:@"state"])
            location.state = [locationDictionary objectForKey:@"state"];
        else
            location.state = @"";
        
        if ([locationDictionary objectForKey:@"country"])
            location.country = [locationDictionary objectForKey:@"country"];
        else
            location.country = @"";
        
            location.formattedAddress = [locationDictionary objectForKey:@"formattedAddress"];

        
    }
    else
    {
        location.address = @"";
        location.crossStreet = @"";
        location.lat = @"";
        location.lng = @"";
        location.postalCode = @"";
        location.cc = @"";
        location.city = @"";
        location.state = @"";
        location.country = @"";
        
    }
    
}

@end

